package ru.tsc.ichaplygina.taskmanager.repository;

public class UserRepositoryTest {

   /* private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private List<User> userList;

    @NotNull
    private IUserRepository userRepository;

    @NotNull
    private Connection connection;

    @Before
    @SneakyThrows
    public void initRepository() {
        @NotNull final ConnectionService connectionService = new ConnectionService(new PropertyService());
        userList = new ArrayList<>();
        connection = connectionService.getConnection();
        userRepository = new UserRepository(connection);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("test user" + i);
            user.setEmail("user@" + i);
            user.setRole(Role.USER);
            user.setPasswordHash("123");
            try {
                userRepository.add(user);
                connection.commit();
            } catch (@NotNull final Exception e) {
                connection.rollback();
                throw e;
            }
            userList.add(user);
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testAdd() {
        try {
            @NotNull final User user = new User();
            user.setLogin("test user 666");
            user.setPasswordHash("123");
            user.setEmail("666");
            user.setRole(Role.USER);
            userRepository.add(user);
            connection.commit();
            Assert.assertNotNull(userRepository.findById(user.getId()));
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testAddAll() {
        int expectedNumberOfEntries = userRepository.getSize() + NUMBER_OF_ENTRIES;
        @NotNull List<User> userList = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("test user add " + i);
            user.setPasswordHash("123");
            user.setEmail("666" + i);
            user.setRole(Role.USER);
            userList.add(user);
        }
        try {
            userRepository.addAll(userList);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testAddAllNull() {
        try {
            userRepository.addAll(null);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testClear() {
        final int expectedNumberOfEntries = 0;
        try {
            userRepository.clear();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindAll() {
        final int expectedNumberOfEntries = userRepository.getSize();
        @NotNull final List<User> actualUserList = userRepository.findAll();
        Assert.assertEquals(expectedNumberOfEntries, actualUserList.size());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByIdPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findById(user.getId()));
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByIdNegative() {
        @NotNull final String id = NumberUtil.generateId();
        Assert.assertNull(userRepository.findById(id));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByLoginPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findByLogin(user.getLogin()));
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByLoginNegative() {
        @NotNull final String login = "unknown_login";
        Assert.assertNull(userRepository.findByLogin(login));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByEmailPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findByEmail(user.getEmail()));
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindByEmailNegative() {
        @NotNull final String email = "unknown_email";
        Assert.assertNull(userRepository.findByEmail(email));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindIdByLoginPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertNotNull(userRepository.findIdByLogin(user.getLogin()));
            Assert.assertEquals(user.getId(), userRepository.findIdByLogin(user.getLogin()));
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testFindIdByLoginNegative() {
        @NotNull final String login = "unknown_login";
        Assert.assertNull(userRepository.findIdByLogin(login));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testGetSize() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsEmpty() {
        Assert.assertFalse(userRepository.isEmpty());
        userRepository.clear();
        Assert.assertTrue(userRepository.isEmpty());
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsFoundByEmailPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertTrue(userRepository.isFoundByEmail(user.getEmail()));
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsFoundByEmailNegative() {
        Assert.assertFalse(userRepository.isFoundByEmail("unknown_email"));
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsFoundByLoginPositive() {
        for (@NotNull final User user : userList) {
            Assert.assertTrue(userRepository.isFoundByLogin(user.getLogin()));
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    public void testIsFoundByLoginNegative() {
        Assert.assertFalse(userRepository.isFoundByLogin("unknown_login"));
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemovePositive() {
        try {
            for (@NotNull final User user : userList) {
                userRepository.remove(user);
                connection.commit();
                Assert.assertNull(userRepository.findById(user.getId()));
            }
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }

    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveNegative() {
        final int expectedSize = userRepository.getSize();
        @NotNull final User userNotFromRepository = new User();
        try {
            userRepository.remove(userNotFromRepository);
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(expectedSize, userRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByIdPositive() {
        try {
            for (@NotNull final User user : userList) {
                @Nullable final User user1 = userRepository.removeById(user.getId());
                connection.commit();
                Assert.assertNotNull(user1);
                @Nullable final User user2 = userRepository.findById(user.getId());
                connection.commit();
                Assert.assertNull(user2);
            }
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByIdNegative() {
        @NotNull final String idNotFromRepository = NumberUtil.generateId();
        try {
            @Nullable final User user = userRepository.removeById(idNotFromRepository);
            connection.commit();
            Assert.assertNull(user);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByLoginPositive() {
        try {
            for (@NotNull final User user : userList) {
                userRepository.removeByLogin(user.getLogin());
                connection.commit();
                Assert.assertNull(userRepository.findById(user.getId()));
            }
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testRemoveByLoginNegative() {
        try {
            userRepository.removeByLogin("unknown_login");
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdatePositive() {
        @NotNull final User userExpected = userList.get(0);
        @NotNull final String userId = userExpected.getId();
        @NotNull final String newLogin = "testnew_login";
        @NotNull final String newPassword = "new_password";
        @NotNull final String newEmail = "new_email";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        userExpected.setLogin(newLogin);
        userExpected.setPasswordHash(newPassword);
        userExpected.setEmail(newEmail);
        userExpected.setRole(newRole);
        userExpected.setFirstName(newFirstName);
        userExpected.setMiddleName(newMiddleName);
        userExpected.setLastName(newLastName);
        try {
            @Nullable final User userActual = userRepository.update(userId, newLogin, newPassword,
                    newEmail, newRole, newFirstName, newMiddleName, newLastName);
            connection.commit();
            Assert.assertEquals(userExpected.getId(), userActual.getId());
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @Test
    @Category(DatabaseCategory.class)
    @SneakyThrows
    public void testUpdateNegative() {
        @NotNull final String userId = NumberUtil.generateId();
        @NotNull final String newLogin = "testtestnew_login";
        @NotNull final String newPassword = "new_password";
        @NotNull final String newEmail = "new_email";
        @NotNull final Role newRole = Role.ADMIN;
        @NotNull final String newFirstName = "new first name";
        @NotNull final String newMiddleName = "new middle name";
        @NotNull final String newLastName = "new last name";
        try {
            @Nullable final User userActual = userRepository.update(userId, newLogin, newPassword,
                    newEmail, newRole, newFirstName, newMiddleName, newLastName);
            connection.commit();
            Assert.assertNull(userActual);
        } catch (@NotNull final Exception e) {
            connection.rollback();
            throw e;
        }
    }

    @After
    @SneakyThrows
    public void closeConnection() {
        try {
            @NotNull final String sql = "DELETE FROM TM_USER WHERE LOGIN LIKE 'test%'";
            @NotNull final Statement statement = connection.createStatement();
            statement.executeUpdate(sql);
            statement.close();
            connection.commit();
        } catch (@NotNull final Exception e) {
            connection.rollback();
        } finally {
            connection.close();
        }
    }*/

}
