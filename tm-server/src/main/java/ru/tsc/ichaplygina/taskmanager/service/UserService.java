package ru.tsc.ichaplygina.taskmanager.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.IUserRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.IPropertyService;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.dto.UserDTO;
import ru.tsc.ichaplygina.taskmanager.enumerated.Role;
import ru.tsc.ichaplygina.taskmanager.exception.empty.EmailEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.LoginEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.PasswordEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserExistsWithEmailException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserExistsWithLoginException;
import ru.tsc.ichaplygina.taskmanager.exception.entity.UserNotFoundException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IncorrectCredentialsException;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static ru.tsc.ichaplygina.taskmanager.util.HashUtil.salt;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;

public final class UserService extends AbstractService<UserDTO> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(@NotNull final IConnectionService connectionService, @NotNull final IPropertyService propertyService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @Override
    @SneakyThrows
    public final void add(@NotNull final String login, @NotNull final String password, @NotNull final String email, @NotNull final Role role,
                          @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        if (isEmptyString(email)) throw new EmailEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            if (!isEmptyString(userRepository.findIdByLogin(login))) throw new UserExistsWithLoginException(login);
            if (!isEmptyString(userRepository.findIdByEmail(email))) throw new UserExistsWithEmailException(email);
            @NotNull final UserDTO user = new UserDTO(login, Objects.requireNonNull(salt(password, propertyService)), email, firstName, middleName, lastName, role);
            userRepository.add(user.getId(), user.getLogin(), user.getPasswordHash(), user.getEmail(), user.getRole(),
                    user.getFirstName(), user.getMiddleName(), user.getLastName(), user.isLocked());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public final void add(@NotNull final UserDTO user) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            if (!isEmptyString(userRepository.findIdByLogin(user.getLogin())))
                throw new UserExistsWithLoginException(user.getLogin());
            if (!isEmptyString(userRepository.findIdByEmail(user.getEmail())))
                throw new UserExistsWithEmailException(user.getEmail());
            userRepository.add(user.getId(), user.getLogin(), user.getPasswordHash(), user.getEmail(), user.getRole(),
                    user.getFirstName(), user.getMiddleName(), user.getLastName(), user.isLocked());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void addAll(@Nullable List<UserDTO> users) {
        if (users == null) return;
        for (final UserDTO user : users) add(user);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            userRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<UserDTO> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            return userRepository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            return userRepository.findById(id);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public final UserDTO findByLogin(@NotNull final String login) {
        return Optional.ofNullable(findUserByLogin(login)).orElseThrow(UserNotFoundException::new);
    }

    @Nullable
    @Override
    public final UserDTO findByLoginForAuthorization(@NotNull final String login) {
        return Optional.ofNullable(findUserByLogin(login)).orElseThrow(IncorrectCredentialsException::new);
    }

    @Nullable
    @SneakyThrows
    private String findIdByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            return userRepository.findIdByLogin(login);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    private UserDTO findUserByLogin(@NotNull final String login) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            return userRepository.findByLogin(login);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            return userRepository.getSize();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmpty() {
        return (getSize() == 0);
    }

    @Override
    public final boolean isPrivilegedUser(@NotNull final String userId) {
        @NotNull final UserDTO user = Optional.ofNullable(findById(userId)).orElseThrow(UserNotFoundException::new);
        return user.getRole().equals(Role.ADMIN);
    }

    @Override
    @SneakyThrows
    public final boolean lockById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            Optional.ofNullable(userRepository.findById(id)).orElseThrow(UserNotFoundException::new);
            boolean result = userRepository.setLockedById(id, true) != 0;
            sqlSession.commit();
            return result;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public final boolean lockByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
            boolean result = userRepository.setLockedByLogin(login, true) != 0;
            sqlSession.commit();
            return result;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @NotNull final UserDTO user = Optional.ofNullable(userRepository.findById(id)).orElseThrow(UserNotFoundException::new);
        try {
            userRepository.removeById(id);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public final UserDTO removeByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        @NotNull final UserDTO user = Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
        try {
            userRepository.removeByLogin(login);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public final void setPassword(@NotNull final String login, @NotNull final String password) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            @NotNull final String id = Optional.ofNullable(findIdByLogin(login)).orElseThrow(UserNotFoundException::new);
            userRepository.setPassword(id, salt(password, propertyService));
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public final void setRole(@NotNull final String login, @NotNull final Role role) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            @Nullable final String id = findIdByLogin(login);
            Optional.ofNullable(id).orElseThrow(UserNotFoundException::new);
            userRepository.setRole(id, role);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public final boolean unlockById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            Optional.ofNullable(userRepository.findById(id)).orElseThrow(UserNotFoundException::new);
            boolean result;
            result = userRepository.setLockedById(id, false) != 0;
            sqlSession.commit();
            return result;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public final boolean unlockByLogin(@NotNull final String login) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            Optional.ofNullable(userRepository.findByLogin(login)).orElseThrow(UserNotFoundException::new);
            boolean result;
            result = userRepository.setLockedByLogin(login, false) != 0;
            sqlSession.commit();
            return result;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public final UserDTO updateById(@NotNull final String id, @NotNull final String login, @NotNull final String password, @NotNull final String email,
                                    @NotNull final Role role, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        if (isEmptyString(password)) throw new PasswordEmptyException();
        if (isEmptyString(email)) throw new EmailEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            @Nullable final UserDTO userFoundWithThisLogin = userRepository.findByLogin(login);
            @Nullable final UserDTO userFoundWithThisEmail = userRepository.findByEmail(email);
            if (userFoundWithThisLogin != null && !id.equals(userFoundWithThisLogin.getId()))
                throw new UserExistsWithLoginException(login);
            if (userFoundWithThisEmail != null && !id.equals(userFoundWithThisEmail.getId()))
                throw new UserExistsWithEmailException(email);
            if (userRepository.update(id, login, password, email, role, firstName, middleName, lastName) == 0)
                throw new UserNotFoundException();
            sqlSession.commit();
            return findById(id);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }

    }

    @Nullable
    @Override
    @SneakyThrows
    public final UserDTO updateByLogin(@NotNull final String login, @NotNull final String password, @NotNull final String email,
                                       @NotNull final Role role, @Nullable final String firstName, @Nullable final String middleName, @Nullable final String lastName) {
        if (isEmptyString(login)) throw new LoginEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
        try {
            @NotNull final String id = Optional.ofNullable(userRepository.findIdByLogin(login)).orElseThrow(UserNotFoundException::new);
            @Nullable final UserDTO user = updateById(id, login, password, email, role, firstName, middleName, lastName);
            sqlSession.commit();
            return user;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
