package ru.tsc.ichaplygina.taskmanager.api;

import org.jetbrains.annotations.NotNull;
import ru.tsc.ichaplygina.taskmanager.api.service.*;

public interface ServiceLocator {

    @NotNull IConnectionService getConnectionService();

    @NotNull IProjectService getProjectService();

    @NotNull IProjectTaskService getProjectTaskService();

    @NotNull IPropertyService getPropertyService();

    @NotNull ITaskService getTaskService();

    @NotNull IUserService getUserService();

}
