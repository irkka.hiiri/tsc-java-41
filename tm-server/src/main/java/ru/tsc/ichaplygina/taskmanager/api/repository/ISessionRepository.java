package ru.tsc.ichaplygina.taskmanager.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.dto.SessionDTO;

import java.util.List;

public interface ISessionRepository {

    @Insert("INSERT INTO tm_session (ID, USER_ID, SIGNATURE, TIME_STAMP) VALUES (#{id}, #{userId}, #{signature}, #{timestamp})")
    void add(@Param("id") @NotNull String id,
             @Param("userId") @NotNull String userId,
             @Param("signature") @Nullable String signature,
             @Param("timestamp") long timestamp);

    @Delete("DELETE FROM tm_session")
    void clear();

    @NotNull
    @Select("SELECT * FROM tm_session")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "signature", column = "SIGNATURE"),
            @Result(property = "timeStamp", column = "TIME_STAMP")
    })
    List<SessionDTO> findAll();

    @Nullable
    @Select("SELECT * FROM tm_session WHERE ID = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "id", column = "ID"),
            @Result(property = "userId", column = "USER_ID"),
            @Result(property = "signature", column = "SIGNATURE"),
            @Result(property = "timeStamp", column = "TIME_STAMP")
    })
    SessionDTO findById(@Param("id") @NotNull String id);

    @Select("SELECT COUNT(1) FROM tm_session")
    int getSize();

    @Delete("DELETE FROM tm_session WHERE ID = #{id}")
    void removeById(@Param("id") @NotNull String id);

}
