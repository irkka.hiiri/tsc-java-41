package ru.tsc.ichaplygina.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.IBusinessEntityService;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.dto.AbstractBusinessEntityDTO;

import java.util.List;

public abstract class AbstractBusinessEntityService<E extends AbstractBusinessEntityDTO> extends AbstractService<E> implements IBusinessEntityService<E> {

    @NotNull
    protected final IUserService userService;

    public AbstractBusinessEntityService(@NotNull final IConnectionService connectionService, @NotNull final IUserService userService) {
        super(connectionService);
        this.userService = userService;
    }

    @Override
    public abstract void add(@NotNull final String userId, @NotNull final String entityName, @Nullable final String entityDescription);

    @Override
    public abstract void clear(final String userId);

    @Nullable
    @Override
    public abstract E completeById(@NotNull final String userId, @NotNull final String entityId);

    @Nullable
    @Override
    public abstract E completeByIndex(@NotNull final String userId, final int entityIndex);

    @Nullable
    @Override
    public abstract E completeByName(@NotNull final String userId, @NotNull final String entityName);

    @NotNull
    @Override
    public abstract List<E> findAll(@NotNull final String userId);

    @NotNull
    @Override
    public abstract List<E> findAll(@NotNull final String userId, @Nullable final String sortBy);

    @Nullable
    @Override
    public abstract E findById(@NotNull final String userId, @Nullable final String entityId);

    @Nullable
    @Override
    public abstract E findByIndex(@NotNull final String userId, final int entityIndex);

    @Nullable
    @Override
    public abstract E findByName(@NotNull final String userId, @NotNull final String entityName);

    @Nullable
    @Override
    public abstract String getId(@NotNull final String userId, final int entityIndex);

    @Nullable
    @Override
    public abstract String getId(@NotNull final String userId, @NotNull final String entityName);

    @Override
    public abstract int getSize(@NotNull final String userId);

    @Override
    public abstract boolean isEmpty(@NotNull final String userId);

    @Nullable
    @Override
    public abstract E removeById(@NotNull final String userId, @NotNull final String entityId);

    @Nullable
    @Override
    public abstract E removeByIndex(@NotNull final String userId, final int entityIndex);

    @Nullable
    @Override
    public abstract E removeByName(@NotNull final String userId, @NotNull final String entityName);

    @Nullable
    @Override
    public abstract E startById(@NotNull final String userId, @NotNull final String entityId);

    @Nullable
    @Override
    public abstract E startByIndex(@NotNull final String userId, final int entityIndex);

    @Nullable
    @Override
    public abstract E startByName(@NotNull final String userId, @NotNull final String entityName);

    @Nullable
    @Override
    public abstract E updateById(@NotNull final String userId,
                                 @NotNull final String entityId,
                                 @NotNull final String entityName,
                                 @Nullable final String entityDescription);

    @Nullable
    @Override
    public abstract E updateByIndex(@NotNull final String userId,
                                    final int entityIndex,
                                    @NotNull final String entityName,
                                    @Nullable final String entityDescription);

}
