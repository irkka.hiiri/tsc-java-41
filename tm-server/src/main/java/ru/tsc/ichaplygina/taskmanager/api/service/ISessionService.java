package ru.tsc.ichaplygina.taskmanager.api.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.IService;
import ru.tsc.ichaplygina.taskmanager.dto.SessionDTO;

public interface ISessionService extends IService<SessionDTO> {

    void closeSession(@NotNull SessionDTO session);

    SessionDTO openSession(@NotNull String login, @NotNull String password);

    @SneakyThrows
    void validatePrivileges(@NotNull String userId);

    @SneakyThrows
    void validateSession(@Nullable SessionDTO session);
}
