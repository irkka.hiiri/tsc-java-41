package ru.tsc.ichaplygina.taskmanager.api.service;

import ru.tsc.ichaplygina.taskmanager.api.IBusinessEntityService;
import ru.tsc.ichaplygina.taskmanager.dto.ProjectDTO;

public interface IProjectService extends IBusinessEntityService<ProjectDTO> {

}
