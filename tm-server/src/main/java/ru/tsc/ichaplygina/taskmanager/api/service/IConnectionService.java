package ru.tsc.ichaplygina.taskmanager.api.service;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull EntityManagerFactory factory();

    @NotNull SqlSession getSqlSession();

    @NotNull SqlSessionFactory getSqlSessionFactory();
}
