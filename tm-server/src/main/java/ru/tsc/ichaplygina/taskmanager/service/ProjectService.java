package ru.tsc.ichaplygina.taskmanager.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.ichaplygina.taskmanager.api.repository.IProjectRepository;
import ru.tsc.ichaplygina.taskmanager.api.service.IConnectionService;
import ru.tsc.ichaplygina.taskmanager.api.service.IProjectService;
import ru.tsc.ichaplygina.taskmanager.api.service.IUserService;
import ru.tsc.ichaplygina.taskmanager.dto.ProjectDTO;
import ru.tsc.ichaplygina.taskmanager.enumerated.Status;
import ru.tsc.ichaplygina.taskmanager.exception.empty.IdEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.empty.NameEmptyException;
import ru.tsc.ichaplygina.taskmanager.exception.incorrect.IndexIncorrectException;

import java.sql.Timestamp;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ru.tsc.ichaplygina.taskmanager.util.ComparatorUtil.getComparator;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isEmptyString;
import static ru.tsc.ichaplygina.taskmanager.util.ValidationUtil.isInvalidListIndex;

public final class ProjectService extends AbstractBusinessEntityService<ProjectDTO> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService, @NotNull final IUserService userService) {
        super(connectionService, userService);
    }

    @Override
    @SneakyThrows
    public void add(@NotNull final ProjectDTO project) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull final String id = project.getId();
        @NotNull final String name = project.getName();
        @Nullable final String description = project.getDescription();
        @NotNull final String status = project.getStatus().toString();
        @NotNull final Timestamp created = new Timestamp(project.getCreated().getTime());
        @Nullable final Timestamp started = project.getDateStart() == null ? null : new Timestamp(project.getDateStart().getTime());
        @Nullable final Timestamp completed = project.getDateFinish() == null ? null : new Timestamp(project.getDateFinish().getTime());
        @NotNull final String userId = project.getUserId();
        try {
            projectRepository.add(id, name, description, status, created, started, completed, userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void add(@NotNull final String userId, @NotNull final String name, @Nullable final String description) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final ProjectDTO project = new ProjectDTO(name, description, userId);
        add(project);
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<ProjectDTO> projectList) {
        if (projectList == null) return;
        for (final ProjectDTO project : projectList) add(project);
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.clear();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear(final String userId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            if (userService.isPrivilegedUser(userId)) projectRepository.clear();
            else projectRepository.clearForUser(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO completeById(@NotNull final String userId, @NotNull final String projectId) {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull final String status = Status.COMPLETED.toString();
        @NotNull final Timestamp timestamp = new Timestamp(new Date().getTime());
        try {
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    projectRepository.completeProjectById(projectId, status, timestamp) :
                    projectRepository.completeProjectByIdForUser(userId, projectId, status, timestamp);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return findById(userId, projectId);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO completeByIndex(@NotNull final String userId, final int index) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull final String status = Status.COMPLETED.toString();
        @NotNull final Timestamp timestamp = new Timestamp(new Date().getTime());
        try {
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    projectRepository.completeProjectByIndex(index, status, timestamp) :
                    projectRepository.completeProjectByIndexForUser(userId, index, status, timestamp);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return findByIndex(userId, index);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO completeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull final String status = Status.COMPLETED.toString();
        @NotNull final Timestamp timestamp = new Timestamp(new Date().getTime());
        try {
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    projectRepository.completeProjectByName(name, status, timestamp) :
                    projectRepository.completeProjectByNameForUser(userId, name, status, timestamp);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return findByName(userId, name);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return projectRepository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return userService.isPrivilegedUser(userId) ?
                    projectRepository.findAll() : projectRepository.findAllForUser(userId);
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@NotNull final String userId, @Nullable final String sortBy) {
        @NotNull final Comparator<ProjectDTO> comparator = getComparator(sortBy);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return userService.isPrivilegedUser(userId) ?
                    projectRepository.findAll().stream().sorted(comparator).collect(Collectors.toList()) :
                    projectRepository.findAllForUser(userId).stream().sorted(comparator).collect(Collectors.toList());
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return projectRepository.findById(id);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findById(@NotNull final String userId, @Nullable final String projectId) {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return userService.isPrivilegedUser(userId) ?
                    projectRepository.findById(projectId) : projectRepository.findByIdForUser(userId, projectId);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findByIndex(@NotNull final String userId, final int entityIndex) {
        if (isInvalidListIndex(entityIndex, getSize())) throw new IndexIncorrectException(entityIndex + 1);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return userService.isPrivilegedUser(userId) ?
                    projectRepository.findByIndex(entityIndex) : projectRepository.findByIndexForUser(userId, entityIndex);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findByName(@NotNull final String userId, @NotNull final String entityName) {
        if (isEmptyString(entityName)) throw new NameEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return userService.isPrivilegedUser(userId) ?
                    projectRepository.findByName(entityName) : projectRepository.findByNameForUser(userId, entityName);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, final int index) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return userService.isPrivilegedUser(userId) ?
                    projectRepository.getIdByIndex(index) : projectRepository.getIdByIndexForUser(userId, index);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getId(@NotNull final String userId, @NotNull final String entityName) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return userService.isPrivilegedUser(userId) ?
                    projectRepository.getIdByName(entityName) : projectRepository.getIdByNameForUser(userId, entityName);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return projectRepository.getSize();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public int getSize(@NotNull final String userId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return userService.isPrivilegedUser(userId) ?
                    projectRepository.getSize() : projectRepository.getSizeForUser(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean isEmpty() {
        return getSize() == 0;
    }

    @Override
    @SneakyThrows
    public boolean isEmpty(@NotNull final String userId) {
        return getSize(userId) == 0;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeById(@NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            @Nullable final ProjectDTO project = findById(id);
            final int affectedRows = projectRepository.removeById(id);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeById(@NotNull final String userId, @NotNull final String id) {
        if (isEmptyString(id)) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            @Nullable final ProjectDTO project = findById(userId, id);
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    projectRepository.removeById(id) :
                    projectRepository.removeByIdForUser(userId, id);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeByIndex(@NotNull final String userId, final int index) {
        if (isInvalidListIndex(index, getSize())) throw new IndexIncorrectException(index + 1);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            @Nullable final ProjectDTO project = findByIndex(userId, index);
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    projectRepository.removeByIndex(index) :
                    projectRepository.removeByIndexForUser(userId, index);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeByName(@NotNull final String userId, @NotNull final String name) {
        if (isEmptyString(name)) throw new NameEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            @Nullable final ProjectDTO project = findByName(userId, name);
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    projectRepository.removeByName(name) :
                    projectRepository.removeByNameForUser(userId, name);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO startById(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull final String status = Status.IN_PROGRESS.toString();
        @NotNull final Timestamp timestamp = new Timestamp(new Date().getTime());
        try {
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    projectRepository.completeProjectById(projectId, status, timestamp) :
                    projectRepository.completeProjectByIdForUser(userId, projectId, status, timestamp);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return findById(userId, projectId);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO startByIndex(@NotNull final String userId, final int index) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull final String status = Status.IN_PROGRESS.toString();
        @NotNull final Timestamp timestamp = new Timestamp(new Date().getTime());
        try {
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    projectRepository.completeProjectByIndex(index, status, timestamp) :
                    projectRepository.completeProjectByIndexForUser(userId, index, status, timestamp);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return findByIndex(userId, index);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO startByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        @NotNull final String status = Status.IN_PROGRESS.toString();
        @NotNull final Timestamp timestamp = new Timestamp(new Date().getTime());
        try {
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    projectRepository.completeProjectByName(name, status, timestamp) :
                    projectRepository.completeProjectByNameForUser(userId, name, status, timestamp);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return findByName(userId, name);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO updateById(@NotNull final String userId,
                                 @NotNull final String projectId,
                                 @NotNull final String projectName,
                                 @Nullable final String projectDescription) {
        if (isEmptyString(projectId)) throw new IdEmptyException();
        if (isEmptyString(projectName)) throw new NameEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            final int affectedRows = userService.isPrivilegedUser(userId) ?
                    projectRepository.update(projectId, projectName, projectDescription) :
                    projectRepository.updateForUser(userId, projectId, projectName, projectDescription);
            sqlSession.commit();
            if (affectedRows == 0) return null;
            return findById(projectId);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public ProjectDTO updateByIndex(@NotNull final String userId,
                                    final int entityIndex,
                                    @NotNull final String entityName,
                                    @Nullable final String entityDescription) {
        if (isInvalidListIndex(entityIndex, getSize(userId))) throw new IndexIncorrectException(entityIndex + 1);
        @Nullable final String entityId = Optional.ofNullable(getId(userId, entityIndex)).orElse(null);
        if (entityId == null) return null;
        return updateById(userId, entityId, entityName, entityDescription);
    }


}
